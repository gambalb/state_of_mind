# State of Mind #

This is a small application that can help you understand yourself better.

## Description ##

This application allows you to look at your every day activities and adjust them to achieve better life experience.

All you need to do is to:

- visit web page once a day
- take a minute to think about what happened that day
- give estimate of that day on 1 to 5 scale
- optionally make some notes (things worth to mention and recall afterward)

Application collects your data and can display it in more tangible way like in form of trend or bar graph and list of entry cards.

This simple activity help you dramatically increase control over your life.

## Demo ##

http://arjester.pythonanywhere.com/