# -*- coding: utf-8 -*-
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status

from authentication.models import Account
from authentication.serializers import AccountSerializer
from entries.models import Entry
from entries.serializers import EntrySerializer


class EntriesTestCase(TestCase):
    def setUp(self):
        self.csrf_client = APIClient(enforce_csrf_checks=True)

        self.username = 'test'
        self.email = 'test@test.com'
        self.password = 'test'
        data = {
            "username": self.username,
            "email": self.email,
            "password": self.password
        }

        serializer = AccountSerializer(data=data)
        if serializer.is_valid():
            self.user = Account.objects.create_user(**serializer.validated_data)

        self.authenticated_client = APIClient(enforce_csrf_checks=True)
        self.authenticated_client.force_authenticate(user=self.user)

        self.entry_date = "2015-08-01"
        entry_data = {
            "date": self.entry_date,
            "rating": 3,
            "good_note": "good_note",
            "bad_note": "bad_note"
        }
        entry_serializer = EntrySerializer(data=entry_data)

        if entry_serializer.is_valid():
            self.entry = entry_serializer.save(author=self.user)

    def test_get_account(self):
        user = Account.objects.latest('created_at')

        self.assertEqual(user.username, 'test')

    def test_get_entry_passed(self):
        response = self.authenticated_client.get("/api/v1/entries/{}/".format(self.entry_date), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_entry_not_authorised(self):
        response = self.csrf_client.get("/api/v1/entries/{}/".format(self.entry_date), format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_entry_not_exist(self):
        response = self.authenticated_client.get("/api/v1/entries/2001-01-01/", format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_entry_passed(self):
        response = self.authenticated_client.post("/api/v1/entries/",
                                                  data={"date": "2015-08-02", "rating": 3},
                                                  format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_entry_with_existing_date(self):
        response = self.authenticated_client.post("/api/v1/entries/",
                                                  data={"date": "2015-08-01", "rating": 3},
                                                  format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_entry_update_note(self):
        response = self.authenticated_client.put("/api/v1/entries/{}/".format(self.entry_date),
                                                 data={"good_note": "new_good_note"},
                                                 format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_entry_update_note_unicode(self):
        unicode_note = "новая заметка"
        response = self.authenticated_client.put("/api/v1/entries/{}/".format(self.entry_date),
                                                 data={"good_note": unicode_note},
                                                 format='json')

        entry = Entry.objects.get(author=self.user, date=self.entry_date)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(entry.good_note, unicode_note)

    def test_entry_update_not_existing_note(self):
        response = self.authenticated_client.put("/api/v1/entries/2001-01-01/",
                                                 data={"good_note": "new_good_note"},
                                                 format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_list_entries(self):
        response = self.authenticated_client.get("/api/v1/entries/", format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_entry_get_earliest_one(self):
        response = self.authenticated_client.get("/api/v1/entries/earliest_entry/", format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)



