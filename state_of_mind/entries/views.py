from django.shortcuts import get_object_or_404
from rest_framework import permissions, viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated

from entries.models import Entry
from entries.serializers import EntrySerializer
from rest_framework import filters
import django_filters


class EntryFilter(django_filters.FilterSet):
    min_date = django_filters.DateFilter(name="date", lookup_type='gte')
    max_date = django_filters.DateFilter(name="date", lookup_type='lte')

    class Meta:
        model = Entry
        fields = ['date', 'min_date', 'max_date']


class EntryViewSet(viewsets.ModelViewSet):
    queryset = Entry.objects.select_related('author').order_by('-date')
    serializer_class = EntrySerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = EntryFilter
    lookup_field = "date"

    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        This view should return a list of all the entries
        for the currently authenticated user.
        """
        user = self.request.user
        return Entry.objects.filter(author=user).order_by('-date')

    def create(self, request):
        serializer = self.serializer_class(data=request.data)

        serializer.is_valid(raise_exception=True)
        queryset = self.get_queryset().filter(date=serializer.data['date'])
        if queryset:
            return Response({'msg': 'entry with date {} allready exists for current user'
                            .format(serializer.data['date'])},
                            status=status.HTTP_400_BAD_REQUEST)

        serializer.save(author=request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, date=None):
        data = request.data
        data['date'] = date
        queryset = self.get_queryset()
        entry = get_object_or_404(queryset, date=date)
        serializer = self.get_serializer(entry, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save(author=request.user)
        return Response(serializer.data)

    @list_route(methods=['get'])
    def earliest_entry(self, request):
        last_entry = self.get_queryset().reverse()[0]
        serializer = self.serializer_class(last_entry)
        return Response(serializer.data)





