# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0002_auto_20150720_1211'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='note',
            name='entry',
        ),
        migrations.DeleteModel(
            name='Note',
        ),
    ]
