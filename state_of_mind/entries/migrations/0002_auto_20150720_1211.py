# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='bad_note',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='entry',
            name='good_note',
            field=models.TextField(default=''),
        ),
    ]
