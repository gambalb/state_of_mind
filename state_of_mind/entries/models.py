from django.db import models
from authentication.models import Account


class Entry(models.Model):
    author = models.ForeignKey(Account)
    date = models.DateField()
    rating = models.IntegerField(default=3)
    good_note = models.TextField(blank=True, default="")
    bad_note = models.TextField(blank=True, default="")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('author', 'date',)

    def __unicode__(self):
        return '{0}'.format(self.date)

