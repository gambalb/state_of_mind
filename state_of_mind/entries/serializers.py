from rest_framework import serializers

from authentication.serializers import AccountSerializer
from entries.models import Entry


class EntrySerializer(serializers.ModelSerializer):
    author = AccountSerializer(read_only=True, required=False)
    rating = serializers.IntegerField(max_value=5, min_value=0)

    class Meta:
        model = Entry
        validators = []
        fields = ('id', 'author', 'date', 'good_note', 'bad_note', 'created_at', 'updated_at', 'rating')
        read_only_fields = ('id', 'created_at', 'updated_at')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(EntrySerializer, self).get_validation_exclusions()

        return exclusions + ['author']

    def create(self, validated_data):
        return Entry.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.rating = validated_data.get('rating', instance.rating)
        instance.good_note = validated_data.get('good_note', instance.good_note)
        instance.bad_note = validated_data.get('bad_note', instance.bad_note)

        instance.save()
        return instance

