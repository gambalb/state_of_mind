(function () {
  'use strict';

  angular
    .module('state_of_mind.routes', ['ngRoute', ]);

  angular
    .module('state_of_mind.config', []);

  angular
    .module('state_of_mind', [
      'state_of_mind.config',
      'state_of_mind.routes',
      'state_of_mind.authentication',
      'state_of_mind.layout',
      //'state_of_mind.posts',
      //'state_of_mind.profiles',
      'state_of_mind.utils',
      'state_of_mind.entry',
      'state_of_mind.graph',
      'monospaced.elastic',
      'nvd3'
    ])
    .run(run);

  run.$inject = ['$http'];

  /**
  * @name run
  * @desc Update xsrf $http headers to align with Django's defaults
  */
  function run($http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
    $.material.init();
  }
})();

