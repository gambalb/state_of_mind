(function () {
    'use strict';

    angular
        .module('state_of_mind.authentication.controllers', []);

    angular
        .module('state_of_mind.authentication.services', ['ngCookies']);

    angular
        .module('state_of_mind.authentication', [
            'state_of_mind.authentication.controllers',
            'state_of_mind.authentication.services'
        ]);
})();