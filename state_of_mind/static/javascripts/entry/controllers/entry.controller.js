/**
 * EntryController
 * @namespace state_of_mind.entry.controllers
 */
(function () {
    'use strict';

    angular
        .module('state_of_mind.entry.controllers')
        .controller('EntryController', EntryController);

    EntryController.$inject = ['$scope', '$routeParams', '$location', '$timeout', 'Entry', 'Snackbar'];

    /**
     * @namespace EntryController
     */
    function EntryController($scope, $routeParams, $location, $timeout, Entry, Snackbar) {
        var vm = this;


        //--------------------------------------------------------------------------------------------------------------
        // RATING SECTION
        //--------------------------------------------------------------------------------------------------------------
        vm.max = 5;
        vm.isReadonly = false;
        vm.savedRating = undefined;

        vm.hoveringOver = hoveringOver;
        /**
         * @name hoveringOver
         * @desc changes overStar and state values based on given hovered value
         * @param value value of currently hovered star
         */
        function hoveringOver(value) {
            var states = {
                0: "",
                1: "Awful",
                2: "Bad",
                3: "Normal",
                4: "Good",
                5: "Great"
            };

            vm.overStar = value;
            vm.state = states[value];

        }

        /**
         * @name updateRating
         * @desc Sends request to update rating value of current entry on server
         * @param current
         * @param prev
         */
        function updateRating(current, prev) {
            if ((current != prev) && (prev!== undefined)) {

                Entry.update(dateToString(vm.date), {"rating": current}).then(updateSuccess(), updateFailure);
            }
        }

        /**
         * @name updateSuccess
         * @desc Catch successful updating
         */
        function updateSuccess(data, status, headers, config){
            // ToDo
            console.log("Success!");
        }

        /**
         * @name updateFailure
         * @desc Notify user that updating failed
         */
        function updateFailure(data, status, headers, config){
            // ToDo
            console.log("Failure!");
            Snackbar.error('Failed to update rating value.');
        }

        //--------------------------------------------------------------------------------------------------------------
        // TEXT SECTIONS
        //--------------------------------------------------------------------------------------------------------------

        vm.savingStates = {
            'saving':'Saving',
            'saved': 'All changes saved',
            'failed': 'Failed to save',
            'modified':'Modified',
            'hide': ''
        };
        var NotificationDuration = 3000; // milliseconds (3000 ==> 3 seconds)

        //--------------------------------------------------------------------------------------------------------------
        // GOOD TEXT SECTION
        //--------------------------------------------------------------------------------------------------------------

        vm.goodText = "";
        vm.goodTextSaved = "";
        vm.goodTextState = vm.savingStates['hide'];

        var goodMessageTimer;
        var goodSavingTimer;

        /**
         * @name goodTextSave
         * @desc Update good note if text was changed
         */
        function goodTextSave(){
            if ((vm.goodText !== vm.goodTextSaved) && (vm.goodTextState != vm.savingStates['saving'])) {

                vm.goodTextState = vm.savingStates['saving'];
                //var data = vm.goodText;
                //$timeout(function (data) {
                //    console.log(data);
                //    Entry.update(dateToString(vm.date), {"good_note": data}).then(goodTextUpdateSuccessFn, goodTextUpdateErrorFn)
                //}, 5000, true, data);
                Entry.update(dateToString(vm.date), {"good_note": vm.goodText}).then(goodTextUpdateSuccessFn,
                    goodTextUpdateErrorFn)
            }
        }

        vm.goodChanged = goodChanged;
        /**
         * @name goodChanged
         * @desc Start countdown on good text change. Restart timer on new change
         */
        function goodChanged() {
            if (goodSavingTimer) {
                $timeout.cancel(goodSavingTimer);
            }
            if (vm.goodText !== vm.goodTextSaved) {
                vm.goodTextState = vm.savingStates['modified'];
                goodSavingTimer = $timeout(goodTextSave, 3000);
            } else if (vm.goodTextState == vm.savingStates['modified']){
                vm.goodTextState = vm.savingStates['hide'];
            }
        }

        vm.goodBlur = goodBlur;
        /**
         * @name goodBlur
         * @desc Save good text on blur event
         * @param $event
         */
        function goodBlur($event) {
            goodTextSave();
        }

        /**
         * @name goodTextUpdateSuccessFn
         * @desc Notify user that all changes where saved successfully. Update last saved text.
         * @param data
         */
        function goodTextUpdateSuccessFn(data){
            vm.goodTextSaved = data.data.good_note;
            vm.goodTextState = vm.savingStates['saved'];
            goodTextSave();
            goodHideNotification()
        }

        /**
         * @name goodHideNotification
         * @desc Hide notification when timeout is expired
         */
        function goodHideNotification() {
            if (goodMessageTimer) {
                $timeout.cancel(goodMessageTimer);
            }
            goodMessageTimer = $timeout(function () {
                if (vm.goodTextState == vm.savingStates['saved']){
                    vm.goodTextState = vm.savingStates['hide'];
                }

            }, NotificationDuration);
        }

        /**
         * @name goodTextUpdateErrorFn
         * @desc Notify user that saving failed
         */
        function goodTextUpdateErrorFn(){
            vm.goodTextState = vm.savingStates['hide'];
            vm.goodText = vm.goodTextSaved;
            Snackbar.error('Failed to save.');
        }

        //--------------------------------------------------------------------------------------------------------------
        // BAD TEXT SECTION
        //--------------------------------------------------------------------------------------------------------------
        vm.badText = "";
        vm.badTextSaved = "";
        vm.badTextState = vm.savingStates['hide'];

        var badMessageTimer;
        var badSavingTimer;
        /**
         * @name badTextSave
         * @desc Update bad note if text was changed
         */
        function badTextSave(){
            if ((vm.badText !== vm.badTextSaved) && (vm.badTextState != vm.savingStates['saving'])) {

                vm.badTextState = vm.savingStates['saving'];
                //var data = vm.badText;
                //$timeout(function (data) {
                //    console.log(data);
                //    Entry.update(dateToString(vm.date), {"bad_note": data}).then(badTextUpdateSuccessFn, badTextUpdateErrorFn)
                //}, 5000, true, data);
                Entry.update(dateToString(vm.date), {"bad_note": vm.badText}).then(badTextUpdateSuccessFn,
                    badTextUpdateErrorFn)
            }
        }

        vm.badChanged = badChanged;
        /**
         * @name badChanged
         * @desc Start countdown on bad text change. Restart timer on new change
         */
        function badChanged() {
            if (badSavingTimer) {
                $timeout.cancel(badSavingTimer);
            }
            if (vm.badText !== vm.badTextSaved) {
                vm.badTextState = vm.savingStates['modified'];
                badSavingTimer = $timeout(badTextSave, 3000);
            } else if (vm.badTextState == vm.savingStates['modified']){
                vm.badTextState = vm.savingStates['hide'];
            }
        }

        vm.badBlur = badBlur;
        /**
         * @name badBlur
         * @desc Save bad text on blur event
         * @param $event
         */
        function badBlur($event) {
            badTextSave();
        }

        /**
         * @name badTextUpdateSuccessFn
         * @desc Notify user that all changes where saved successfully. Update last saved text.
         * @param data
         */
        function badTextUpdateSuccessFn(data, status, headers, config){
            vm.badTextSaved = data.data.bad_note;
            vm.badTextState = vm.savingStates['saved'];
            badTextSave();
            badHideNotification()
        }

        /**
         * @name badHideNotification
         * @desc Hide notification when timeout is expired
         */
        function badHideNotification() {
            if (badMessageTimer) {
                $timeout.cancel(badMessageTimer);
            }
            badMessageTimer = $timeout(function () {
                if (vm.badTextState == vm.savingStates['saved']){
                    vm.badTextState = vm.savingStates['hide'];
                }

            }, NotificationDuration);
        }

        /**
         * @name badTextUpdateErrorFn
         * @desc Notify user that saving failed
         */
        function badTextUpdateErrorFn(data, status, headers, config){
            vm.badTextState = vm.savingStates['hide'];
            vm.badText = vm.badTextSaved;
            Snackbar.error('Failed to save.');
        }


        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf state_of_mind.entry.controllers.EntryController
         */
        function activate() {
            if ($routeParams.date == "today"){
                vm.date = new Date();
            } else {
                if (new Date($routeParams.date) > new Date()){
                    $location.path('/entry/today/');
                } else {
                    vm.date = new Date($routeParams.date);
                }

            }
            setupDaysNavigation();

            Entry.get(dateToString(vm.date)).then(entrySuccessFn, entryGetErrorFn);

            $scope.$watchCollection(function () { return $scope.vm.rate; }, updateRating);
        }

        /**
         * @name setupDaysNavigation
         * @desc sets values of previous and following days
         */
        function setupDaysNavigation(){
            if (new Date(vm.date).setHours(0,0,0,0) < new Date().setHours(0,0,0,0)){
                vm.next_date = dateToString(new Date((new Date(vm.date)).setDate(vm.date.getDate()+1)));
            }
            vm.prev_date = dateToString(new Date((new Date(vm.date)).setDate(vm.date.getDate()-1)));
        }


        /**
         * @name entrySuccessFn
         * @desc Update current entry info
         */
        function entrySuccessFn(data, status, headers, config) {
            vm.date = new Date(data.data.date);
            vm.savedRating = data.data.rating;
            vm.rate = data.data.rating;
            vm.goodTextSaved = data.data.good_note;
            vm.goodText = data.data.good_note;
            vm.badTextSaved = data.data.bad_note;
            vm.badText = data.data.bad_note;
        }


        /**
         * @name entryGetErrorFn
         * @desc If entry non exist try to create default one. If there was some other error notify user.
         */
        function entryGetErrorFn(data, status, headers, config) {
            if (data.status == 404) {
                var content = {"date": dateToString(vm.date), "rating":3};
                Entry.create(content).then(entrySuccessFn, entryCreateErrorFn);
            } else {
                Snackbar.error('That entry does not exist.');
            }
        }

        /**
         * @name entryCreateErrorFn
         * @desc Notify user
         */
        function entryCreateErrorFn(data, status, headers, config) {
            Snackbar.error('Unable to create entry ' + date);
        }

        //--------------------------------------------------------------------------------------------------------------
        // Helpers
        //--------------------------------------------------------------------------------------------------------------
        /**
         * @name dateToString
         * @desc Converts date object to string in format: YYYY-MM-DD
         * @param date date object to convert
         * @returns {string}
         */
        function dateToString(date) {
            var local = new Date(date);
            local.setMinutes(date.getMinutes() - date.getTimezoneOffset());
            return local.toJSON().slice(0, 10);
        }
    }
})();