/**
 * EntryController
 * @namespace state_of_mind.entry.controllers
 */
(function () {
    'use strict';

    angular
        .module('state_of_mind.entry.controllers')
        .controller('EntriesController', EntriesController);

    EntriesController.$inject = ['$scope', 'Entry', 'Snackbar'];

    /**
     * @namespace EntryController
     */
    function EntriesController($scope, Entry, Snackbar) {
        var vm = this;
        var ROOT_LINK = '/entry/';
        vm.isMore = false;
        vm.lastDateExist = undefined;
        vm.lastDateShown = undefined;

        vm.entries = [];

        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf state_of_mind.entry.controllers.EntriesController
         */
        function activate() {
            var today = new Date(new Date().setHours(0,0,0,0));
            var eleven_days_ago = new Date((new Date(today)).setDate(today.getDate()-11));
            // var last_date = twelve_days_ago;

            Entry.listByDates(dateToString(eleven_days_ago), dateToString(today))
                .then(successCallbackCreator(eleven_days_ago, today), entryAllErrorFn);
            Entry.getLast().then(entryLastSuccessFn, entryLastErrorFn);

            /**
             * @name entryLastSuccessFn
             * @desc Update date of earliest existing entry
             * @param data
             */
            function entryLastSuccessFn(data, status, headers, config) {
                vm.lastDateExist = new Date(data.data.date)
            }

            /**
             * @name entryLastSuccessFn
             * @desc Notify user that date of last entry wasn't obtained
             */
            function entryLastErrorFn(data, status, headers, config) {
                Snackbar.error('Unable to get last entry');
            }
        }

        /**
         * @name successCallbackCreator
         * @desc Creates callback function for successful request to get new entries
         * @param start_date date of last entry specified in request
         * @param end_date date of earliest entry specified in request
         * @returns {Function}
         */
        function successCallbackCreator(start_date, end_date) {
            return function(data, status, headers, config) {
                vm.entries.push.apply(vm.entries, generateData(data.data, start_date, end_date));
                vm.lastDateShown = start_date;
            }
        }

        /**
         * @name entryAllErrorFn
         * @desc Notify user that there was a problem receiving entries
         */
        function entryAllErrorFn(data, status, headers, config) {
            Snackbar.error('Unable to get list of entries');
        }

        /**
         * @name generateData
         * @desc Generates list with data for each entry card from startDate to endDate
         * @param data list of entries received from server between startDate and endDate
         * @param startDate date of last entry specified in request
         * @param endDate date of earliest entry specified in request
         * @returns {Array}
         */
        function generateData(data,startDate, endDate){
            var dates = getDates(startDate, endDate).reverse();
            var entries = data;

            var entriesMap = entries.map(function(data, i) {
                return data.date;
            });

            return dates.map(function(data, i) {
                var entry;
                var j = entriesMap.indexOf(data);

                if (j !== -1){
                    entry = entries[j];
                    return {
                        'date': new Date(entry.date),
                        'rate': entry.rating,
                        'goodText': entry.good_note,
                        'badText': entry.bad_note,
                        'link': ROOT_LINK + entry.date + "/",
                        'max': entry.rating
                    }
                } else {
                    return {
                        'date': new Date(data),
                        'rate': 0,
                        'goodText': "",
                        'badText': "",
                        'link': ROOT_LINK + dateToString(new Date(data)) + "/",
                        'max': 5
                    }
                }
            })
        }

        /**
         * @name addDays
         * @desc Add certain number of days to date
         * @param date original date value
         * @param days number of days that need to be added to date
         * @returns {Date}
         */
        function addDays(date, days) {
            var dat = new Date(date);
            dat.setDate(dat.getDate() + days);
            return dat;
        }

        /**
         * @name getDates
         * @desc Creates array with all dates(days) between startDate and stopDate
         * @param startDate
         * @param stopDate
         * @returns {Array}
         */
        function getDates(startDate, stopDate) {
            var dateArray = [];
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push( dateToString(new Date (currentDate)) );
                currentDate = addDays(currentDate, 1);
            }
            return dateArray;
        }

        /**
         * @name dateToString
         * @desc Converts date object to string in format: YYYY-MM-DD
         * @param date date object to convert
         * @returns {string}
         */
        function dateToString(date) {
            var local = new Date(date);
            local.setMinutes(date.getMinutes() - date.getTimezoneOffset());
            return local.toJSON().slice(0, 10);
        }

        /**
         * @name showMore
         * @desc Makes request to receive 12 more entries starting from lastDateShown
         */
        vm.showMore = function (){
            var to = new Date((new Date(vm.lastDateShown)).setDate(vm.lastDateShown.getDate() - 1));
            var from = new Date((new Date(to)).setDate(to.getDate()-11));
            Entry.listByDates(dateToString(from), dateToString(to))
                .then(successCallbackCreator(from, to), entryAllErrorFn);
        };

    }
})();