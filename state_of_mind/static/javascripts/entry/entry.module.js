(function () {
  'use strict';


  angular
    .module('state_of_mind.entry.services', []);

  angular
    .module('state_of_mind.entry.controllers', ['ui.bootstrap', 'ngAnimate']);

  angular
    .module('state_of_mind.entry.directives', ['ui.bootstrap']);

  angular
    .module('state_of_mind.entry', [
      'state_of_mind.entry.controllers',
      'state_of_mind.entry.directives',
      'state_of_mind.entry.services',
      'ui.bootstrap'
    ]);
})();