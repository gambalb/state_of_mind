/**
* Post
* @namespace state_of_mind.posts.directives
*/
(function () {
  'use strict';

  angular
    .module('state_of_mind.entry.directives')
    .directive('entryCard', entryCard);

  /**
  * @namespace entryCard
  */
  function entryCard() {
    /**
    * @name directive
    * @desc The directive to be returned
    * @memberOf state_of_mind.entry.directives.entryCard
    */
    var directive = {
      restrict: 'E',
      scope: {
        entry: '='
      },
      templateUrl: '/static/templates/entry/entry-card.html'
    };

    return directive;
  }
})();