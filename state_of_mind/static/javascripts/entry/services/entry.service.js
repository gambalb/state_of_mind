/**
 * Entry
 * @namespace state_of_mind.entry.services
 */
(function () {
    'use strict';

    angular
        .module('state_of_mind.entry.services')
        .factory('Entry', Entry);

    Entry.$inject = ['$http'];

    /**
     * @namespace Entry
     * @returns {Factory}
     */
    function Entry($http) {
        var Entry = {
            all: all,
            create: create,
            get: get,
            getLast: getLast,
            update: update,
            listByDates: listByDates
        };

        return Entry;

        ////////////////////

        /**
         * @name all
         * @desc Get all Entry
         * @returns {Promise}
         * @memberOf state_of_mind.entries.services.Entry
         */
        function all() {
            return $http.get('/api/v1/entries/');
        }


        /**
         * @name listByDates
         * @desc Get dates in selected range Entry
         * @returns {Promise}
         * @memberOf state_of_mind.entries.services.Entry
         */
        function listByDates(date_from, date_to) {
            var url = '/api/v1/entries/';
            var filters = '';

            if (date_from) {
                filters += ((filters) ? "&&" : "?") + 'min_date=' + date_from;
            }

            if (date_to) {
                filters += ((filters) ? "&&" : "?") + 'max_date=' + date_to;
            }

            return $http.get(url + filters)
        }


        /**
         * @name create
         * @desc Create a new Post
         * @param {string} content The content of the new Post
         * @returns {Promise}
         * @memberOf state_of_mind.entries.services.Entry
         */
        function create(content) {
            return $http.post('/api/v1/entries/', content);
        }

        /**
         * @name get
         * @desc Get the Entry of a given user
         * @param {string} username The username to get Entry for
         * @returns {Promise}
         * @memberOf state_of_mind.entries.services.Entry
         */
        function get(date) {
            return $http.get('/api/v1/entries/' + date + "/");
        }

        /**
         *
         * @returns {HttpPromise}
         */
        function getLast() {
            return $http.get('/api/v1/entries/earliest_entry/');
        }

        /**
         *
         * @param date
         * @param content
         * @returns {HttpPromise}
         */
        function update(date, content) {
            return $http.put('/api/v1/entries/' + date + "/", content);
        }
    }
})();