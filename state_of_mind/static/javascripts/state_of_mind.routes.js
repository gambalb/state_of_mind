(function () {
  'use strict';

  angular
    .module('state_of_mind.routes')
    .config(config);

  config.$inject = ['$routeProvider'];

  /**
  * @name config
  * @desc Define valid application routes
  */
  function config($routeProvider) {
    $routeProvider.when('/register', {
      controller: 'RegisterController', 
      controllerAs: 'vm',
      templateUrl: '/static/templates/authentication/register.html'
    }).when('/login', {
      controller: 'LoginController',
      controllerAs: 'vm',
      templateUrl: '/static/templates/authentication/login.html'
    }).when('/', {
      controller: 'IndexController',
      controllerAs: 'vm',
      templateUrl: '/static/templates/layout/index.html'
    }).when('/entry/:date', {
      controller: 'EntryController',
      controllerAs: 'vm',
      templateUrl: '/static/templates/entry/entry.html'
    }).when('/entries/', {
      controller: 'EntriesController',
      controllerAs: 'vm',
      templateUrl: '/static/templates/entry/entries.html'
    }).when('/graph/', {
      controller: 'GraphController',
      controllerAs: 'vm',
      templateUrl: '/static/templates/graph/graph.html'
    }).when('/+:username', {
    //  controller: 'ProfileController',
    //  controllerAs: 'vm',
    //  templateUrl: '/static/templates/profiles/profile.html'
    //}).when('/+:username/settings', {
    //  controller: 'ProfileSettingsController',
    //  controllerAs: 'vm',
    //  templateUrl: '/static/templates/profiles/settings.html'
    }).otherwise('/');
  }
})();