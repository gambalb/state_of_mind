(function () {
  'use strict';

  angular
    .module('state_of_mind.profiles', [
      'state_of_mind.profiles.controllers',
      'state_of_mind.profiles.services'
    ]);

  angular
    .module('state_of_mind.profiles.controllers', []);

  angular
    .module('state_of_mind.profiles.services', []);
})();