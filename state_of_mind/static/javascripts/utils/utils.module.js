(function () {
  'use strict';

  angular
    .module('state_of_mind.utils', [
      'state_of_mind.utils.services'
    ]);

  angular
    .module('state_of_mind.utils.services', []);
})();