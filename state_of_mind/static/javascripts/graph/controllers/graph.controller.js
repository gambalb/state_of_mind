/**
 * GraphController
 * @namespace state_of_mind.graph.controllers
 */
(function () {
    'use strict';

    angular
        .module('state_of_mind.graph.controllers')
        .controller('GraphController', GraphController);

    GraphController.$inject = ['$scope', 'Entry', '$timeout', 'Snackbar'];

    /**
     * @namespace GraphController
     */
    function GraphController($scope,  Entry, $timeout, Snackbar) {
        var vm = this;
        var ratingsMap = {
            // 0: "",
            1: "Awful",
            2: "Bad",
            3: "Normal",
            4: "Good",
            5: "Great"
        };
        vm.entries = [];
        vm.showLineChart = true;
        vm.showBarChart = false;



        vm.options = {
            chart: {
                type: 'lineWithFocusChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 40,
                    bottom: 80,
                    left: 55
                },
                transitionDuration: 500,
                xAxis: {
                    tickFormat: function(d){
                        return d3.time.format('%m/%d/%y')(new Date(d));
                    },
                    tickPadding: 15
                },
                x2Axis: {
                    tickFormat: function(d){
                        return d3.time.format('%m/%d/%y')(new Date(d));
                    },
                    tickPadding: 15
                },
                yAxis: {
                    axisLabel: 'Rating',
                    tickFormat: function(d){
                        return d3.format('')(d);
                    },
                    rotateYLabel: true,
                    axisLabelDistance: 45,
                    tickValues: [0, 1, 2, 3, 4, 5]

                },
                y2Axis: {
                    tickFormat: function(d){
                        return d3.format(',')(d);
                    },
                    tickValues: [0, 5]
                },
                interpolate: "cardinal",
                isArea: "true",
                color: lineColorFunction(),
                noData: "Loalding data...",
                margin2: {
                    top:  0,
                    right:  40,
                    bottom:  50,
                    left:  40
                },
                showLegend: false
            }
        };


        var lineColorArray = ['#4caf50'];

        /**
         * @name lineColorFunction
         * @desc Returns function that provides correct colors graph
         * @returns {Function}
         */
        function lineColorFunction() {
            return function(d, i) {
                return lineColorArray[i];
            };
        }

        /**
         * @name barColorArray
         * @desc Returns function that provides correct colors graph
         * @returns {Function}
         */
        var barColorArray = ['#735834', '#C78523', '#FFC107', '#8BC34A', '#4caf50'];
        function barColorFunction() {
            return function(d, i) {
                return barColorArray[i];
            };
        }

        vm.optionsBar = {
            chart: {
                type: 'discreteBarChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 55
                },
                x: function(d){return d.label;},
                y: function(d){return d.value;},
                showValues: true,
                valueFormat: function(d){
                    return d3.format(',f')(d);
                },
                transitionDuration: 500,
                xAxis: {
                    tickPadding: 15
                },
                yAxis: {
                    tickFormat: function(d){
                        return d3.format('')(d);
                    },
                    axisLabel: 'Days',
                    axisLabelDistance: 30
                },
                noData: "Loalding data...",
                color: barColorFunction()
            }
        };

        /**
         * @name selectLineChart
         * @desc switch to line chart
         */
        vm.selectLineChart = function (){
            vm.showLineChart = true;
            vm.showBarChart = false;

            // ugly hack but couldn't find anything better
            $timeout(function (){vm.refreshCharts();}, 100);
        };

        vm.refreshCharts = function () {
            for (var i = 0; i < nv.graphs.length; i++) {
                nv.graphs[i].update();
            }
        };

        /**
         * @name selectBarChart
         * @desc switch to bar chart
         */
        vm.selectBarChart = function (){
            vm.showLineChart = false;
            vm.showBarChart = true;

            // ugly hack but couldn't find anything better
            $timeout(function (){vm.refreshCharts();}, 100);
        };

        vm.data = [];
        vm.dataBar = [];

        activate();


        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf state_of_mind.graph.controllers.GraphController
         */
        function activate() {
            Entry.all().then(entryAllSuccessFn, entryAllErrorFn);

            /**
             * @name entryAllSuccessFn
             * @desc from received data generate data for line and bar graphs
             * @param data
             */
            function entryAllSuccessFn(data) {
                vm.data = generateData(data.data);
                vm.dataBar = generateDataBar(data.data);
            }

            /**
             * @name entryAllErrorFn
             * @desc Notify user that entries from server wasn't obtained
             */
            function entryAllErrorFn() {
                Snackbar.error('Unable to get list of entries');
            }

            /**
             * @name generateData
             * @desc Generates data for line graph
             * @param entries list of entries from server
             * @returns {*[]}
             */
            function generateData(entries) {
                var values =  entries.map(function(data, i) {
                    return {'x': new Date(data.date), 'y': data.rating};

                });
                return [{
                    "key": "Rating",
                    "values": values
                }]
            }

            /**
             * @name generateDataBar
             * @desc Generates data for bar graph
             * @param entries list of entries from server
             * @returns {*[]}
             */
            function generateDataBar(entries) {
                var keys = [], count = [], ratings, bars = {}, prev, values = [];
                ratings = entries.map(function(data, i) {
                    return data.rating;
                });

                ratings.sort();
                for ( var j = 0; j < ratings.length; j++ ) {
                    if ( ratings[j] !== prev ) {
                        keys.push(ratings[j]);
                        count.push(1);
                    } else {
                        count[count.length-1]++;
                    }
                    prev = ratings[j];
                }

                _.each(keys,function(k,i){bars[k] = count[i];});

                for (var i=1;i<=5; i++) {
                    if (ratingsMap[i]) {
                        values.push({"label": ratingsMap[i],
                            "value": ((bars[i]) ? bars[i] : 0)
                        })
                    }
                }

                return [{
                    "key": "Cumulative ratings",
                    "values": values
                }]
            }
        }
    }
})();