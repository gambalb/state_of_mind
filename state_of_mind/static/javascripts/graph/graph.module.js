(function () {
  'use strict';


  angular
    .module('state_of_mind.graph.services', []);

  angular
    .module('state_of_mind.graph.controllers', ['ui.bootstrap', 'ngAnimate', 'nvd3']);

  angular
    .module('state_of_mind.graph.directives', []);

  angular
    .module('state_of_mind.graph', [
      'state_of_mind.graph.controllers',
      'state_of_mind.graph.directives',
      'state_of_mind.graph.services',
      'ui.bootstrap'
    ]);
})();