(function () {
  'use strict';

  angular
    .module('state_of_mind.layout', [
      'state_of_mind.layout.controllers'
    ]);

  angular
    .module('state_of_mind.layout.controllers', []);
})();