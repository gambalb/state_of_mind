/**
* IndexController
* @namespace state_of_mind.layout.controllers
*/
(function () {
  'use strict';

  angular
    .module('state_of_mind.layout.controllers')
    .controller('IndexController', IndexController);

  IndexController.$inject = ['$scope', '$location', 'Authentication', 'Snackbar'];

  /**
  * @namespace IndexController
  */
  function IndexController($scope, $location, Authentication, Snackbar) {
    var vm = this;

    vm.isAuthenticated = Authentication.isAuthenticated();
    vm.posts = [];

    activate();

    /**
    * @name activate
    * @desc Actions to be performed when this controller is instantiated
    * @memberOf state_of_mind.layout.controllers.IndexController
    */
    function activate() {
      if (vm.isAuthenticated){
        $location.path('/entry/today/');
      }
    }
  }
})();